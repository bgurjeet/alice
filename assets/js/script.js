$(document).ready(function() {
    var error = $('.error');
    // ******code for timer******
    $(".counter")
        .countdown("2019/02/09", function(event) {
            $('.day').text(
                event.strftime('%D')
            );
            $('.hour').text(
                event.strftime('%H')
            );
            $('.minute').text(
                event.strftime('%M')
            );
            $('.seconds').text(
                event.strftime('%S')
            );
        });
    // ******** code for form email validation****
    $('#email').blur(function() {
        var email = $('#email').val();
        var emailreg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        if (email.match(emailreg)) {
            $(error).removeClass('show');
            $('input').removeClass('error');
        } else {
        		$('input').addClass('error');
            $(error).addClass('show');
        }
    });
    $('button').click(function(e) {
        var email = $('#email').val();
        var emailreg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        if (email.match(emailreg)) {
            $(error).removelass('show');
            $('input').removeClass('error');
        } else {
        	$('input').addClass('error');
            $(error).addClass('show');
            e.preventDefault();
        }
    });
});